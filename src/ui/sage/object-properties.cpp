// SPDX-License-Identifier: GPL-2.0-or-later
/**
 * @file Object properties dialog.
 */
/*
 * Inkscape, an Open Source vector graphics editor
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright (C) 2012 Kris De Gussem <Kris.DeGussem@gmail.com>
 * c++ version based on former C-version (GPL v2+) with authors:
 *   Lauris Kaplinski <lauris@kaplinski.com>
 *   bulia byak <buliabyak@users.sf.net>
 *   Johan Engelen <goejendaagh@zonnet.nl>
 *   Abhishek Sharma
 */

#include "object-properties.h"

#include <glibmm/i18n.h>

#include <gtkmm/grid.h>

#include "desktop.h"
#include "document-undo.h"
#include "document.h"
#include "inkscape.h"
#include "verbs.h"
#include "style.h"
#include "style-enums.h"

#include "isac-menu.h"
#include "object/sp-image.h"
#include "ui/icon-names.h"
#include "widgets/sp-attribute-widget.h"

namespace Inkscape {
    namespace UI {
        namespace Dialog {

            ObjectProperties::ObjectProperties()
                :  DialogBase("/dialogs/object/", "ObjectProperties")
                  , _blocked(false)
                  , _current_item(nullptr)
                  , apply_bttn(_("_Apply"), 1)	//HSH - create apply button
                  , ok_bttn(_("_Ok"), 1)	//HSH - create ok button
                  , cancel_bttn(_("_Cancel"), 1) //HSH - create cancel button

                  , _desktop(nullptr)


                  {
                      //initialize labels for the table at the bottom of the dialog

                      _init();
                  }

            ObjectProperties::~ObjectProperties()
            {
                _subselection_changed_connection.disconnect();
                _selection_changed_connection.disconnect();
            }

            void ObjectProperties::_init()
            {
                set_spacing(0);

                this->sage_widget = Glib::wrap(sp_item_widget_new ());
                pack_start(*sage_widget, TRUE, TRUE, 0);

                apply_bttn.signal_clicked().connect(sigc::mem_fun(*this, &ObjectProperties::apply_button));
                this->ok_bttn.signal_clicked().connect(sigc::mem_fun(*this, &ObjectProperties::ok_button));
                //this->cancel_bttn.signal_clicked().connect(sigc::bind(_signal_response.make_slot(), GTK_RESPONSE_CLOSE));

                button_row.pack_start(apply_bttn, TRUE, TRUE, 0);
                button_row.pack_start(ok_bttn, TRUE, TRUE, 0);
                //button_row.pack_start(cancel_bttn, TRUE, TRUE, 0);
                pack_start(button_row, FALSE, FALSE, 0);

                show_all();
                show_all_children();
                //	widget_setup();
                update_entries();
            }

            void ObjectProperties::update_entries()
            {
                if (_blocked || !getDesktop()) {
                   return;
                }

                Inkscape::Selection *selection = SP_ACTIVE_DESKTOP->getSelection();

                if (!selection->singleItem()) {
                    set_sensitive (false);
                    _current_item = nullptr;
                    //no selection anymore or multiple objects selected, means that we need
                    //to close the connections to the previously selected object
                    //_attr_table->clear();
                    return;
                } else {
                    set_sensitive (true);
                }

                SPItem *item = selection->singleItem();
                if (_current_item == item)
                {
                    //otherwise we would end up wasting resources through the modify selection
                    //callback when moving an object (endlessly setting the labels and recreating _attr_table)
                    return;
                }
                _blocked = true;
                //_cb_aspect_ratio.set_active(g_strcmp0(item->getAttribute("preserveAspectRatio"), "none") != 0);
                //_cb_lock.set_active(item->isLocked());           /* Sensitive */
                //_cb_hide.set_active(item->isExplicitlyHidden()); /* Hidden */

                if (item->cloned) {
                    /* ID */
                    //_entry_id.set_text("");
                    //_entry_id.set_sensitive(FALSE);
                    //_label_id.set_text(_("Ref"));

                    /* Label */
                    //_entry_label.set_text("");
                    //_entry_label.set_sensitive(FALSE);
                    //_label_label.set_text(_("Ref"));

                } else {
                    // SPObject *obj = static_cast<SPObject*>(item);

                    /* ID */
                    //_entry_id.set_text(obj->getId() ? obj->getId() : "");
                    //_entry_id.set_sensitive(TRUE);
                    //_label_id.set_markup_with_mnemonic(_("_ID:") + Glib::ustring(" "));

                    /* Label */
                    //char const *currentlabel = obj->label();
                    //char const *placeholder = "";
                    //if (!currentlabel) {
                    //    currentlabel = "";
                    //    placeholder = obj->defaultLabel();
                    //}
                    //_entry_label.set_text(currentlabel);
                    //_entry_label.set_placeholder_text(placeholder);
                    //_entry_label.set_sensitive(TRUE);

                    /* Title */
                    //gchar *title = obj->title();
                    //if (title) {
                    //    _entry_title.set_text(title);
                    //    g_free(title);
                    //}
                    //else {
                    //    _entry_title.set_text("");
                    //}
                    //_entry_title.set_sensitive(TRUE);

                    /* Image Rendering */
                    //if (SP_IS_IMAGE(item)) {
                    //    _combo_image_rendering.show();
                    //    _label_image_rendering.show();
                    //    _combo_image_rendering.set_active(obj->style->image_rendering.value);
                    //    if (obj->getAttribute("inkscape:svg-dpi")) {
                    //        _spin_dpi.set_value(std::stod(obj->getAttribute("inkscape:svg-dpi")));
                    //        _spin_dpi.show();
                    //        _label_dpi.show();
                    //    } else {
                    //        _spin_dpi.hide();
                    //        _label_dpi.hide();
                    //    }
                    //} else {
                    //    _combo_image_rendering.hide();
                    //    _combo_image_rendering.unset_active();
                    //    _label_image_rendering.hide();
                    //    _spin_dpi.hide();
                    //    _label_dpi.hide();
                    //}

                    /* Description */
                    //gchar *desc = obj->desc();
                    //if (desc) {
                    //    _tv_description.get_buffer()->set_text(desc);
                    //    g_free(desc);
                    //} else {
                    //    _tv_description.get_buffer()->set_text("");
                    //}
                    //_ft_description.set_sensitive(TRUE);

                    //if (_current_item == nullptr) {
                    //    _attr_table->set_object(obj, _int_labels, _int_attrs, (GtkWidget*) _exp_interactivity.gobj());
                    //} else {
                    //    _attr_table->change_object(obj);
                    //}
                    //_attr_table->show_all();
                }
                sp_item_widget_setup (GTK_WIDGET(sage_widget->gobj()), SP_ACTIVE_DESKTOP->getSelection());
                _current_item = item;
                _blocked = false;
            }

            void ObjectProperties::apply_button()
            {
                color_deletelastrow();
                clone_deletelastrow();
                isac_update_syntax(NULL, GTK_WIDGET(sage_widget->gobj()));
                sp_item_widget_label_changed (NULL, GTK_WIDGET(sage_widget->gobj()));
            }

            void ObjectProperties::ok_button()
            {
                apply_button();
                //HideWindow();
            }

            void ObjectProperties::_labelChanged()
            {
                if (_blocked) {
                    return;
                }

                SPItem *item = SP_ACTIVE_DESKTOP->getSelection()->singleItem();
                g_return_if_fail (item != nullptr);

                _blocked = true;

                /* Retrieve the label widget for the object's id */
                /*gchar *id = g_strdup(_entry_id.get_text().c_str());
                  g_strcanon(id, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_.:", '_');
                  if (g_strcmp0(id, item->getId()) == 0) {
                  _label_id.set_markup_with_mnemonic(_("_ID:") + Glib::ustring(" "));
                  } else if (!*id || !isalnum (*id)) {
                  _label_id.set_text(_("Id invalid! "));
                  } else if (SP_ACTIVE_DOCUMENT->getObjectById(id) != nullptr) {
                  _label_id.set_text(_("Id exists! "));
                  } else {
                  SPException ex;
                  _label_id.set_markup_with_mnemonic(_("_ID:") + Glib::ustring(" "));
                  SP_EXCEPTION_INIT(&ex);
                  item->setAttribute("id", id, &ex);
                  DocumentUndo::done(SP_ACTIVE_DOCUMENT, SP_VERB_DIALOG_ITEM, _("Set object ID"));
                  }
                  g_free(id);*/

                /* Retrieve the label widget for the object's label */
                //Glib::ustring label = _entry_label.get_text();

                /* Give feedback on success of setting the drawing object's label
                 * using the widget's label text
                 */
                //SPObject *obj = static_cast<SPObject*>(item);
                //char const *currentlabel = obj->label();
                //if (label.compare(currentlabel ? currentlabel : "")) {
                //    obj->setLabel(label.c_str());
                //    DocumentUndo::done(SP_ACTIVE_DOCUMENT, SP_VERB_DIALOG_ITEM,
                //            _("Set object label"));
                //}

                /* Retrieve the title */
                //if (obj->setTitle(_entry_title.get_text().c_str())) {
                //    DocumentUndo::done(SP_ACTIVE_DOCUMENT, SP_VERB_DIALOG_ITEM,
                //            _("Set object title"));
                //}

                /* Retrieve the DPI */
                //if (SP_IS_IMAGE(obj)) {
                //    Glib::ustring dpi_value = Glib::ustring::format(_spin_dpi.get_value());
                //    obj->setAttribute("inkscape:svg-dpi", dpi_value);
                //    DocumentUndo::done(SP_ACTIVE_DOCUMENT, SP_VERB_DIALOG_ITEM, _("Set image DPI"));
                //}

                /* Retrieve the description */
                //Gtk::TextBuffer::iterator start, end;
                //_tv_description.get_buffer()->get_bounds(start, end);
                //Glib::ustring desc = _tv_description.get_buffer()->get_text(start, end, TRUE);
                //if (obj->setDesc(desc.c_str())) {
                //    DocumentUndo::done(SP_ACTIVE_DOCUMENT, SP_VERB_DIALOG_ITEM,
                //            _("Set object description"));
                //}

                _blocked = false;
            }

            void ObjectProperties::_imageRenderingChanged()
            {
                if (_blocked) {
                    return;
                }

                SPItem *item = SP_ACTIVE_DESKTOP->getSelection()->singleItem();
                g_return_if_fail (item != nullptr);

                _blocked = true;

                //Glib::ustring scale = _combo_image_rendering.get_active_text();

                // We should unset if the parent computed value is auto and the desired value is auto.
                //SPCSSAttr *css = sp_repr_css_attr_new();
                //sp_repr_css_set_property(css, "image-rendering", scale.c_str());
                //Inkscape::XML::Node *image_node = item->getRepr();
                //if (image_node) {
                //    sp_repr_css_change(image_node, css, "style");
                //    DocumentUndo::done(SP_ACTIVE_DOCUMENT, SP_VERB_DIALOG_ITEM,
                //            _("Set image rendering option"));
                //}
                //sp_repr_css_attr_unref(css);

                _blocked = false;
            }

            void ObjectProperties::_sensitivityToggled()
            {
                if (_blocked) {
                    return;
                }

                SPItem *item = SP_ACTIVE_DESKTOP->getSelection()->singleItem();
                g_return_if_fail(item != nullptr);

                _blocked = true;
                //item->setLocked(_cb_lock.get_active());
                //DocumentUndo::done(SP_ACTIVE_DOCUMENT, SP_VERB_DIALOG_ITEM,
                //                   _cb_lock.get_active() ? _("Lock object") : _("Unlock object"));
                _blocked = false;
            }

            void ObjectProperties::_aspectRatioToggled()
            {
                if (_blocked) {
                    return;
                }

                SPItem *item = SP_ACTIVE_DESKTOP->getSelection()->singleItem();
                g_return_if_fail(item != nullptr);

                _blocked = true;

                //const char *active;
                //if (_cb_aspect_ratio.get_active()) {
                //    active = "xMidYMid";
                //}
                //else {
                //    active = "none";
                //}
                ///* Retrieve the DPI */
                //if (SP_IS_IMAGE(item)) {
                //    Glib::ustring dpi_value = Glib::ustring::format(_spin_dpi.get_value());
                //    item->setAttribute("preserveAspectRatio", active);
                //    DocumentUndo::done(SP_ACTIVE_DOCUMENT, SP_VERB_DIALOG_ITEM, _("Set preserve ratio"));
                //}
                _blocked = false;
            }

            void ObjectProperties::_hiddenToggled()
            {
                if (_blocked) {
                    return;
                }

                SPItem *item = SP_ACTIVE_DESKTOP->getSelection()->singleItem();
                g_return_if_fail(item != nullptr);

                _blocked = true;
                //item->setExplicitlyHidden(_cb_hide.get_active());
                //DocumentUndo::done(SP_ACTIVE_DOCUMENT, SP_VERB_DIALOG_ITEM,
                //           _cb_hide.get_active() ? _("Hide object") : _("Unhide object"));
                _blocked = false;
            }

            void ObjectProperties::update()
            {
                //if (!_app) {
                //    std::cerr << "ObjectProperties::update(): _app is null" << std::endl;
                //    return;
                //}

                SPDesktop *desktop = getDesktop();

                if (!desktop) {
                    return;
                }

                if (this->_desktop != desktop) {
                    if (this->_desktop) {
                        _subselection_changed_connection.disconnect();
                        _selection_changed_connection.disconnect();
                    }
                    this->_desktop = desktop;
                    if (desktop && desktop->selection) {
                        _selection_changed_connection = desktop->selection->connectChanged(
                                sigc::hide(sigc::mem_fun(*this, &ObjectProperties::update_entries))
                                );
                        _subselection_changed_connection = desktop->connectToolSubselectionChanged(
                                sigc::hide(sigc::mem_fun(*this, &ObjectProperties::update_entries))
                                );
                    }
                    update_entries();
                }
            }

            void ObjectProperties::selectionChanged(Selection *selection)
            {
                update_entries();
            }

            void ObjectProperties::desktopReplaced()
            {
                update_entries();
            }

        }
    }
}

/* SAGE CODE */
using Inkscape::DocumentUndo;
static void sp_item_widget_modify_selection( GtkWidget *spw, Inkscape::Selection *selection, guint /*flags*/, GtkWidget *itemw )
{
    sp_item_widget_setup (spw, selection);
}
static void sp_item_widget_change_selection ( GtkWidget *spw, Inkscape::Selection *selection, GtkWidget *itemw )
{
    sp_item_widget_setup (spw, selection);
}

void sp_item_widget_hidden_toggled(GtkWidget *widget, GtkWidget *spw)
{
    if (g_object_get_data (G_OBJECT(spw), "blocked"))
        return;

    SPItem *item = SP_ACTIVE_DESKTOP->getSelection()->singleItem();
    g_return_if_fail (item != NULL);

    g_object_set_data (G_OBJECT (spw), "blocked", GUINT_TO_POINTER (TRUE));

    item->setExplicitlyHidden(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget)));
    
    DocumentUndo::done(SP_ACTIVE_DESKTOP->getDocument(), gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget))? _("Hide object") : _("Unhide object"), INKSCAPE_ICON("dialog-object-properties"));
    //DocumentUndo::done(SP_ACTIVE_DOCUMENT, SP_VERB_DIALOG_ITEM,
    //        gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget))? _("Hide object") : _("Unhide object"));

    g_object_set_data (G_OBJECT (spw), "blocked", GUINT_TO_POINTER (FALSE));
}
void sp_item_widget_label_changed( GtkWidget *widget, GtkWidget *spw )
{
    if (g_object_get_data (G_OBJECT (spw), "blocked"))
        return;

    SPItem *item = SP_ACTIVE_DESKTOP->getSelection()->singleItem();
    g_return_if_fail (item != NULL);

    g_object_set_data (G_OBJECT (spw), "blocked", GUINT_TO_POINTER (TRUE));

    /* Retrieve the label widget for the object's id */
    GtkWidget *id_entry = GTK_WIDGET(g_object_get_data (G_OBJECT (spw), "id"));
    gchar *id = (gchar *) gtk_entry_get_text (GTK_ENTRY (id_entry));
    g_strcanon (id, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_.:", '_');
    GtkWidget *id_label = GTK_WIDGET(g_object_get_data (G_OBJECT (spw), "id_label"));
    if (!strcmp (id, item->getId())) {
        gtk_label_set_markup_with_mnemonic (GTK_LABEL (id_label), _("_ID:"));
    } else if (!*id || !isalnum (*id)) {
        gtk_label_set_text (GTK_LABEL (id_label), _("Id invalid! "));
    } else if (SP_ACTIVE_DOCUMENT->getObjectById(id) != NULL) {
        gtk_label_set_text (GTK_LABEL (id_label), _("Id exists! "));
    } else {
        //SPException ex;
        gtk_label_set_markup_with_mnemonic (GTK_LABEL (id_label), _("_ID:"));
        //SP_EXCEPTION_INIT (&ex);
        item->setAttribute("id", id);
        DocumentUndo::done(SP_ACTIVE_DESKTOP->getDocument(), _("Set object ID"), INKSCAPE_ICON("dialog-object-properties"));   
        //DocumentUndo::done(SP_ACTIVE_DOCUMENT, SP_VERB_DIALOG_ITEM,
        //        _("Set object ID"));
    }

    /* Retrieve the label widget for the object's label */
    GtkWidget *label_entry = GTK_WIDGET(g_object_get_data (G_OBJECT (spw), "label"));
    gchar *label = (gchar *)gtk_entry_get_text (GTK_ENTRY (label_entry));
    g_assert(label != NULL);

    /* Give feedback on success of setting the drawing object's label
     * using the widget's label text
     */
    SPObject *obj = (SPObject*)item;
    if (strcmp (label, obj->defaultLabel())) {
        obj->setLabel(label);
        DocumentUndo::done(SP_ACTIVE_DESKTOP->getDocument(), _("Set object label"), INKSCAPE_ICON("dialog-object-properties"));   
        //DocumentUndo::done(SP_ACTIVE_DOCUMENT, SP_VERB_DIALOG_ITEM,
        //        _("Set object label"));
    }

    /* Retrieve the title */
    GtkWidget *w = GTK_WIDGET(g_object_get_data(G_OBJECT(spw), "title"));
    gchar *title = (gchar *)gtk_entry_get_text(GTK_ENTRY (w));
    if (obj->setTitle(title))
        DocumentUndo::done(SP_ACTIVE_DESKTOP->getDocument(), _("Set object title"), INKSCAPE_ICON("dialog-object-properties"));   
        //DocumentUndo::done(SP_ACTIVE_DOCUMENT, SP_VERB_DIALOG_ITEM,
        //        _("Set object title"));

    /* Retrieve the description */
    GtkTextView *tv = GTK_TEXT_VIEW(g_object_get_data(G_OBJECT(spw), "desc"));
    GtkTextBuffer *buf = gtk_text_view_get_buffer(tv);
    GtkTextIter start, end;
    gtk_text_buffer_get_bounds(buf, &start, &end);
    gchar *desc = gtk_text_buffer_get_text(buf, &start, &end, TRUE);
    if (obj->setDesc(desc))
        DocumentUndo::done(SP_ACTIVE_DESKTOP->getDocument(), _("Set object description"), INKSCAPE_ICON("dialog-object-properties"));   
        //DocumentUndo::done(SP_ACTIVE_DOCUMENT, SP_VERB_DIALOG_ITEM,
        //        _("Set object description"));
    g_free(desc);

    g_object_set_data (G_OBJECT (spw), "blocked", GUINT_TO_POINTER (FALSE));

}

GtkWidget *sp_item_widget_new ()
{
    /**
     * \brief  Creates new instance of item properties widget
     *
     */

    GtkWidget *spw, *vb, *t, *cb, *l, *f, *tf, *pb, *int_expander, *int_label;
    GtkTextBuffer *desc_buffer;

    /* Create container widget */
    spw = gtk_box_new(GTK_ORIENTATION_VERTICAL,4);
    //g_signal_connect ( G_OBJECT (spw), "modify_selection",
    //        G_CALLBACK (sp_item_widget_modify_selection),
    //        spw );
    //g_signal_connect ( G_OBJECT (spw), "change_selection",
    //        G_CALLBACK (sp_item_widget_change_selection),
    //        spw );

    vb = gtk_vbox_new (FALSE, 0);
    gtk_container_add (GTK_CONTAINER (spw), vb);

    t = gtk_table_new (3, 4, FALSE);
    gtk_container_set_border_width(GTK_CONTAINER(t), 4);
    gtk_table_set_row_spacings (GTK_TABLE (t), 4);
    gtk_table_set_col_spacings (GTK_TABLE (t), 4);
    gtk_box_pack_start (GTK_BOX (vb), t, TRUE, TRUE, 0);


    /* Create the label for the title id */
    l = gtk_label_new_with_mnemonic (_("_Title:"));
    gtk_misc_set_alignment (GTK_MISC (l), 1, 0.5);
//    gtk_table_attach ( GTK_TABLE (t), l, 0, 1, 0, 1,(GtkAttachOptions)( GTK_SHRINK | GTK_FILL ),(GtkAttachOptions)0, 0, 0 );
    g_object_set_data (G_OBJECT (spw), "title_label", l);

    /* Create the entry box for the title id */
    tf = gtk_entry_new ();
    gtk_widget_set_tooltip_text(tf, _("The title attribute"));
    gtk_entry_set_max_length (GTK_ENTRY (tf), 64);
//    gtk_table_attach ( GTK_TABLE (t), tf, 1, 2, 0, 1,(GtkAttachOptions)( GTK_EXPAND | GTK_FILL ),(GtkAttachOptions)0, 0, 0 );
    g_object_set_data (G_OBJECT (spw), "title", tf);
    gtk_label_set_mnemonic_widget (GTK_LABEL(l), tf);

    /* Create the label for the description */
    l = gtk_label_new_with_mnemonic (_("_Description:"));
    gtk_misc_set_alignment (GTK_MISC (l), 1, 0.5);
//    gtk_table_attach ( GTK_TABLE (t), l, 0, 1, 0, 1,(GtkAttachOptions)( GTK_SHRINK | GTK_FILL ),(GtkAttachOptions)0, 0, 0 );
    g_object_set_data (G_OBJECT (spw), "desc_label", l);

    /* Create the entry box for the description */
    tf = gtk_text_view_new ();
    gtk_widget_set_tooltip_text(tf, _("The description attribute"));
//    gtk_entry_set_max_length (GTK_ENTRY (tf), 64); text_view does not support
//    gtk_table_attach ( GTK_TABLE (t), tf, 1, 2, 0, 1,(GtkAttachOptions)( GTK_EXPAND | GTK_FILL ),(GtkAttachOptions)0, 0, 0 );
    g_object_set_data (G_OBJECT (spw), "desc", tf);
    gtk_label_set_mnemonic_widget (GTK_LABEL(l), tf);

    /* Create the label for the object id */
    l = gtk_label_new_with_mnemonic (_("_ID:"));
    gtk_misc_set_alignment (GTK_MISC (l), 1, 0.5);
    gtk_table_attach ( GTK_TABLE (t), l, 0, 1, 0, 1,
            (GtkAttachOptions)( GTK_SHRINK | GTK_FILL ),
            (GtkAttachOptions)0, 0, 0 );
    g_object_set_data (G_OBJECT (spw), "id_label", l);

    /* Create the entry box for the object id */
    tf = gtk_entry_new ();
    gtk_widget_set_tooltip_text(tf, _("The id= attribute (only letters, digits, and the characters .-_: allowed)"));
    gtk_entry_set_max_length (GTK_ENTRY (tf), 64);
    gtk_table_attach ( GTK_TABLE (t), tf, 1, 2, 0, 1,
            (GtkAttachOptions)( GTK_EXPAND | GTK_FILL ),
            (GtkAttachOptions)0, 0, 0 );
    g_object_set_data (G_OBJECT (spw), "id", tf);
    gtk_label_set_mnemonic_widget (GTK_LABEL(l), tf);

    // pressing enter in the id field is the same as clicking Set:
    g_signal_connect ( G_OBJECT (tf), "activate", G_CALLBACK (sp_item_widget_label_changed), spw);
    // focus is in the id field initially:
    gtk_widget_grab_focus (GTK_WIDGET (tf));

    /* Create the label for the object label */
    l = gtk_label_new_with_mnemonic (_("_ISAC:"));
    gtk_misc_set_alignment (GTK_MISC (l), 1, 0.5);
    // gtk_table_attach ( GTK_TABLE (t), l, 0, 1, 1, 2,
    //                  (GtkAttachOptions)( GTK_SHRINK | GTK_FILL ),
    //                (GtkAttachOptions)0, 0, 0 );
    g_object_set_data (G_OBJECT (spw), "label_label", l);

    /* Create the entry box for the object label */
    tf = gtk_entry_new ();
    gtk_widget_set_sensitive (GTK_WIDGET (tf), FALSE);
    gtk_widget_set_tooltip_text(tf, _("ISAC syntax")); //ISAC Changed on the tooltip for "ISAC" label
    //gtk_entry_set_max_length (GTK_ENTRY (tf), 256);
    //gtk_table_attach ( GTK_TABLE (t), tf, 1, 2, 1, 2,
    //                 (GtkAttachOptions)( GTK_EXPAND | GTK_FILL ),
    //               (GtkAttachOptions)0, 0, 0 );
    g_object_set_data (G_OBJECT (spw), "label", tf);
    gtk_label_set_mnemonic_widget (GTK_LABEL(l), tf);


    /* Create the frame for interactivity options */
    int_label = gtk_label_new_with_mnemonic (_("_Interactivity"));
    int_expander = gtk_expander_new (NULL);
    gtk_expander_set_label_widget (GTK_EXPANDER (int_expander),int_label);
    g_object_set_data (G_OBJECT (spw), "interactivity", int_expander);
    //gtk_box_pack_start (GTK_BOX (vb), int_expander, FALSE, FALSE, 0);

    /*************************************
      ISAC Features:
      Initializing List
      These list are using for multiple entry field rows in ISAC menu.
      It is necessary to free these list before re-initialize it for 
      repeatitive use or reopen of windows. 
     *************************************/
    isac_menu_init(GTK_TABLE(t), G_OBJECT(spw));
    sage_gui();

    gtk_widget_show_all (spw);

    sp_item_widget_setup (GTK_WIDGET (spw), SP_ACTIVE_DESKTOP->getSelection());

    //gtk_widget_set_size_request(spw, ISAC_DEF_WIN_WIDTH, ISAC_DEF_WIN_HEIGHT);	//HSH - Fix window resize

    return (GtkWidget *) spw;

} 
static void sp_item_widget_setup ( GtkWidget *spw, Inkscape::Selection *selection )
{
    /**
     *  \param selection Selection to use; should not be NULL.
     */

    GtkWidget *w;

    g_assert (selection != NULL);

    if (g_object_get_data (G_OBJECT (spw), "blocked"))
        return;

    if (!selection->singleItem()) {
        gtk_widget_set_sensitive (GTK_WIDGET (spw), FALSE);
        return;
    } else {
        gtk_widget_set_sensitive (GTK_WIDGET (spw), TRUE);
    }

    g_object_set_data (G_OBJECT (spw), "blocked", GUINT_TO_POINTER (TRUE));

    SPItem *item = selection->singleItem();

    /* Sensitive */
    //GtkWidget *w = GTK_WIDGET(g_object_get_data (G_OBJECT (spw), "sensitive"));
    //gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (w), item->isLocked());

    /* Hidden */
    //w = GTK_WIDGET(g_object_get_data (G_OBJECT (spw), "hidden"));
    //gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(w), item->isExplicitlyHidden());

    if (item->cloned) {

        /* ID */
        w = GTK_WIDGET(g_object_get_data (G_OBJECT (spw), "id"));
        gtk_entry_set_text (GTK_ENTRY (w), "");
        gtk_widget_set_sensitive (w, FALSE);
        w = GTK_WIDGET(g_object_get_data (G_OBJECT (spw), "id_label"));
        gtk_label_set_text (GTK_LABEL (w), _("Ref"));

        /* Label */
        w = GTK_WIDGET(g_object_get_data (G_OBJECT (spw), "label"));
        gtk_entry_set_text (GTK_ENTRY (w), "");
        gtk_widget_set_sensitive (w, FALSE);
        w = GTK_WIDGET(g_object_get_data (G_OBJECT (spw), "label_label"));
        gtk_label_set_text (GTK_LABEL (w), _("Ref"));

    } else {
        SPObject *obj = (SPObject*)item;

        /* ID */
        w = GTK_WIDGET(g_object_get_data (G_OBJECT (spw), "id"));
        gtk_entry_set_text (GTK_ENTRY (w), obj->getId());
        gtk_widget_set_sensitive (w, TRUE);
        w = GTK_WIDGET(g_object_get_data (G_OBJECT (spw), "id_label"));
        gtk_label_set_markup_with_mnemonic (GTK_LABEL (w), _("_ID:"));

        /* Label */
        w = GTK_WIDGET(g_object_get_data (G_OBJECT (spw), "label"));
        //gtk_entry_set_text (GTK_ENTRY (w), obj->defaultLabel());
        //gtk_widget_set_sensitive (w, TRUE);
        //ISAC Changed: if object doesnt have valid json syntax, leave it empty during initialization - by dennis*/
        const gchar *def = obj->defaultLabel();

        if(def[0]=='{') //ISAC Changed: If object has a valid json syntax, it shall start with "{"
        {
            gtk_entry_set_text (GTK_ENTRY (w), obj->defaultLabel());
            isac_change_mode(spw);
            isac_json_parse_syntax(def, spw);
        }

        else //ISAC Changed: If object has an invalid/empty syntax, set the ISAC label field to empty.
        {
            gtk_entry_set_text (GTK_ENTRY (w), "");
            isac_change_mode(spw);
        }

        gtk_widget_set_sensitive (w, TRUE);

        /* Title */
        w = GTK_WIDGET(g_object_get_data(G_OBJECT(spw), "title"));
        gchar *title = obj->title();
        if (title) {
            gtk_entry_set_text(GTK_ENTRY(w), title);
            g_free(title);
        }
        else gtk_entry_set_text(GTK_ENTRY(w), "");
        gtk_widget_set_sensitive(w, TRUE);

        /* Description */
        w = GTK_WIDGET(g_object_get_data(G_OBJECT(spw), "desc"));
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(w));
        gchar *desc = obj->desc();
        if (desc) {
            gtk_text_buffer_set_text(buf, desc, -1);
            g_free(desc);
        } else {
            gtk_text_buffer_set_text(buf, "", 0);
        }
        w = GTK_WIDGET(g_object_get_data(G_OBJECT(spw), "desc_frame"));
        gtk_widget_set_sensitive(w, TRUE);

        w = GTK_WIDGET(g_object_get_data(G_OBJECT(spw), "interactivity"));

        /*
           GtkWidget* int_table = GTK_WIDGET(g_object_get_data(G_OBJECT(spw), "interactivity_table"));
           if (int_table){
           gtk_container_remove(GTK_CONTAINER(w), int_table);
           }

           const gchar* int_labels[10] = {"onclick", "onmouseover", "onmouseout", "onmousedown", "onmouseup", "onmousemove","onfocusin", "onfocusout", "onactivate", "onload"};

           int_table = sp_attribute_table_new (obj, 10, int_labels, int_labels);
           gtk_widget_show_all (int_table);
           g_object_set_data(G_OBJECT(spw), "interactivity_table", int_table);

           gtk_container_add (GTK_CONTAINER (w), int_table);
           */

    }
    g_object_set_data (G_OBJECT (spw), "blocked", GUINT_TO_POINTER (FALSE));
}



/*
   Local Variables:
mode:c++
c-file-style:"stroustrup"
c-file-offsets:((innamespace . 0)(inline-open . 0)(case-label . +))
indent-tabs-mode:nil
fill-column:99
End:
*/
// vim: filetype=cpp:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:fileencoding=utf-8:textwidth=99 :
