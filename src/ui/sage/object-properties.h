// SPDX-License-Identifier: GPL-2.0-or-later
/**
 * @file Object properties dialog.
 */
/*
 * Inkscape, an Open Source vector graphics editor
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright (C) 2012 Kris De Gussem <Kris.DeGussem@gmail.com>
 * c++version based on former C-version (GPL v2+) with authors:
 *   Lauris Kaplinski <lauris@kaplinski.com>
 *   bulia byak <buliabyak@users.sf.net>
 *   Johan Engelen <goejendaagh@zonnet.nl>
 *   Abhishek Sharma
 */

#ifndef SEEN_DIALOGS_ITEM_PROPERTIES_H
#define SEEN_DIALOGS_ITEM_PROPERTIES_H


#include <gtkmm/checkbutton.h>
#include <gtkmm/comboboxtext.h>
#include <gtkmm/entry.h>
#include <gtkmm/expander.h>
#include <gtkmm/frame.h>
#include <gtkmm/textview.h>
#include <gtkmm/spinbutton.h>
#include <gtkmm/textview.h>

//#include "ui/widget/panel.h"
#include "ui/widget/scrollprotected.h"
#include "ui/dialog/dialog-base.h"
#include "ui/widget/frame.h"

GtkWidget *sp_item_widget_new (void);
void sp_item_dialog (void);

class SPAttributeTable;
class SPDesktop;
class SPItem;

namespace Gtk {
class Grid;
}

namespace Inkscape {
namespace UI {
namespace Dialog {

/**
 * A dialog widget to show object properties.
 *
 * A widget to enter an ID, label, title and description for an object.
 * In addition it allows to edit the properties of an object.
 */
class ObjectProperties : public DialogBase
{
public:
    ObjectProperties();
    ~ObjectProperties() override;

    static ObjectProperties &getInstance() { return *new ObjectProperties(); }

    /// Updates entries and other child widgets on selection change, object modification, etc.
    // void widget_setup(void);
    void HideWindow();	//HSH
    void update_entries();
    void update() override;
    void selectionChanged(Selection *selection) override;

private:
    bool _blocked;
    SPItem *_current_item; //to store the current item, for not wasting resources
    std::vector<Glib::ustring> _int_attrs;
    std::vector<Glib::ustring> _int_labels;

    SPDesktop *_desktop;
    sigc::connection _selection_changed_connection;
    sigc::connection _subselection_changed_connection;

    Gtk::HBox button_row;		//HSH - Container for bottom buttons
    Gtk::Button cancel_bttn;	//HSH - Cancel button
    Gtk::Button ok_bttn;		//HSH - Ok button
    Gtk::Button apply_bttn;		//HSH - Apply button


    /// Constructor auxiliary function creating the child widgets.
    void _init();

    /// Sets object properties (ID, label, title, description) on user input.
    void _labelChanged();

    /// Callback for 'image-rendering'.
    void _imageRenderingChanged();

    //HSH
    Gtk::Widget * sage_widget;

    /**
     * Applies settings and closes dialog
     */
    void ok_button(void);

    /**
     * Applies settings
     */
    void apply_button(void);

    /**
     * Constructor auxiliary function creating the child widgets.
     */
    void MakeWidget(void);
    
    /// Callback for checkbox Lock.
    void _sensitivityToggled();

    /// Callback for checkbox Hide.
    void _hiddenToggled();

    /// Callback for checkbox Preserve Aspect Ratio.
    void _aspectRatioToggled();

    void desktopReplaced() override;

};

}
}
}


#endif

/*
  Local Variables:
  mode:c++
  c-file-style:"stroustrup"
  c-file-offsets:((innamespace . 0)(inline-open . 0)(case-label . +))
  indent-tabs-mode:nil
  fill-column:99
  End:
*/
// vim: filetype=cpp:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:fileencoding=utf-8:textwidth=99 :
